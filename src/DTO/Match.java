package DTO;

import java.util.ArrayList;

public class Match{

    private int idmatch = 0, idgagnant = 0, idtournoi = 0, placeInArray = 0;
    private int phase = 16;
    //idequipe1, idequipe2
    private String score = "0-0";
    private User joueur1 = new User();
    private User joueur2 = new User();
    public static ArrayList<Match> ListMatch= new ArrayList();
    

    public Match(){

    }

    public Match(User joueur1, User joueur2){
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
        ListMatch.add(this);
    }
    
    public Match(int idmatch, User joueur1, User joueur2, int idtournoi, int idgagnant, String score, int placeInArray, int phase){
        this.idmatch = idmatch;
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
        this.idtournoi = idtournoi;
        this.idgagnant = idgagnant;
        this.score = score;
        this.placeInArray = placeInArray;
        this.phase = phase;
        ListMatch.add(this);
    }

    /**
     * @return int return the idmatch
     */
    public int getIdmatch() {
        return idmatch;
    }

    /**
     * @param idmatch the idmatch to set
     */
    public void setIdmatch(int idmatch) {
        this.idmatch = idmatch;
    }

    /**
     * @return int return the idgagnant
     */
    public int getIdgagnant() {
        return idgagnant;
    }

    /**
     * @param idgagnant the idgagnant to set
     */
    public void setIdgagnant(int idgagnant) {
        this.idgagnant = idgagnant;
    }

    /**
     * @return String return the score
     */
    public String getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * @return User return the joueur1
     */
    public User getJoueur1() {
        return joueur1;
    }

    /**
     * @param joueur1 the joueur1 to set
     */
    public void setJoueur1(User joueur1) {
        this.joueur1 = joueur1;
    }

    /**
     * @return User return the joueur2
     */
    public User getJoueur2() {
        return joueur2;
    }

    /**
     * @param joueur2 the joueur2 to set
     */
    public void setJoueur2(User joueur2) {
        this.joueur2 = joueur2;
    }
    /**
    public int getIdequipe1() {
        return idequipe1;
    }
    * 
    public void setIdequipe1(int idequipe1) {
        this.idequipe1 = idequipe1;
    }

    public int getIdequipe2() {
        return idequipe2;
    }
    
    public void setIdequipe2(int idequipe2) {
        this.idequipe2 = idequipe2;
    }
    */
    
    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }
    
    public int getPlaceInArray() {
        return placeInArray;
    }

    public void setPlaceInArray(int placeInArray) {
        this.placeInArray = placeInArray;
    }

    public int getIdtournoi() {
        return idtournoi;
    }

    public void setIdtournoi(int idtournoi) {
        this.idtournoi = idtournoi;
    }

    public static ArrayList<Match> getListMatch() {
        return ListMatch;
    }

    public void setListMatch(ArrayList<Match> ListMatch) {
        this.ListMatch = ListMatch;
    }
    
    public static ArrayList<Match> getMatchList(){
        return ListMatch;
    }
    
    public ArrayList<Match> matchesByTournament(int id){
        ArrayList<Match> list = new ArrayList();
        for (Match m : ListMatch){
            if(m.getIdtournoi() == id){
                list.add(m);
            }
        }
        return list;
    }
}