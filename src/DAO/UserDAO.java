/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.User;
import java.sql.*;

/**
 *
 * @author antoi
 */
public class UserDAO {
    
    private final Connection con;
    
    public UserDAO(Connection c){
        this.con = c;
    }
    
    public void loadPlayers(){
        ResultSet rs;
        Statement st;
        try{
            st = con.createStatement();
            rs = st.executeQuery("select * from user where idtype = 1");
            while (rs.next()){
                new User(rs.getInt("iduser"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("idtype"), rs.getString("nationalité"));
            }
        }catch(SQLException e){
            System.out.println(e);
        }
    }
    
    public User loadPlayer(int id) {
        ResultSet rset;
        Statement stmt = null;
        try {
            //stmt = connexionBD.createStatement();
            String rqt = "SELECT * FROM user WHERE iduser=?";
            PreparedStatement pst = con.prepareStatement(rqt);
            pst.setInt(1,id);
            rset = pst.executeQuery();
            rset.next();
            User u = new User(rset.getInt("iduser"),rset.getString("nom"),rset.getString("prenom"),rset.getInt("idtype"),rset.getString("nationalité"));
            return u;
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
            return null;
        } finally {
            try {
                // la clause finally est toujours executée, quoi qu'il arrive
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException ex) {
                return null;
            }
        }
    }
}
