package DTO;

import java.util.ArrayList;

public class Tournoi {

    private int id;
    private String titre;
    private boolean isFinished;
    private static Tournoi tournoi;

    public Tournoi() {
        
    }
    
    public Tournoi(int id, String titre) {
        this.id=id;
        this.titre=titre;
        this.isFinished=false;
        tournoi=this;
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    public static Tournoi getTournoi() {
        return tournoi;
    }

    public static void setTournoi(Tournoi tournoi) {
        Tournoi.tournoi = tournoi;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean IsFinished() {
        return isFinished;
    }

}