/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import static Controller.TournoiController.idtournoi;
import java.sql.*;
import DTO.Match;
import DTO.User;
import static java.sql.Types.NULL;
/**
 *
 * @author antoi
 */
public class MatchDAO {
    private final Connection con;
    
    public MatchDAO(Connection c){
        this.con = c;
    }

    public void updateMatch(Match m) throws SQLException {
        PreparedStatement pst = con.prepareStatement("UPDATE rencontre SET idjoueur1=?, idjoueur2=?, idgagnant=?, idtournoi=?, score=?, phase=? where placeinarray=?");
        pst.setInt(1,m.getJoueur1().getIduser());
        pst.setInt(2,m.getJoueur2().getIduser());
        pst.setInt(3,m.getIdgagnant());
        pst.setInt(4,m.getIdtournoi());
        pst.setString(5,m.getScore());
        pst.setInt(6,m.getPhase());
        pst.setInt(7,m.getPlaceInArray());
        pst.executeUpdate();
    }
    
    public void getMatchesFromTournament(int i, UserDAO userDAO) {//crée des objets correspondants aux matchs dans le tournoi en cours dans la table
        PreparedStatement prep = null;
        try {
            
            ResultSet rset;
            String rqt = "SELECT * FROM rencontre WHERE idtournoi=?";
            prep = con.prepareStatement(rqt);
            prep.setInt(1,i);
            rset = prep.executeQuery();
            User u1;
            User u2;
            int e =1;
            while (rset.next()) {
                System.out.println("");
                System.out.println("Itération "+e);
                e++;
                u1 = userDAO.loadPlayer(rset.getInt("idjoueur1"));
                u2 = userDAO.loadPlayer(rset.getInt("idjoueur2")); 
                if(u1 != null && u2 != null) {
                    new Match(rset.getInt("idmatch"), u1, u2, rset.getInt("idtournoi"), rset.getInt("idgagnant"), rset.getString("score"), rset.getInt("placeInArray"), rset.getInt("phase"));
                }
            }
        } catch (SQLException exc) {
            System.out.println(exc.getErrorCode());
        } finally {
            try {
                // la clause finally est toujours executée, quoi qu'il arrive
                if (prep != null) {
                    prep.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getErrorCode());
            }
        }
    }
    
    public int insertMatch(Match m) throws SQLException {
        PreparedStatement pst = con.prepareStatement("INSERT INTO rencontre (idjoueur1,idjoueur2,idgagnant,idtournoi,score,phase,placeinarray) VALUES (?,?,?,?,?,?,?)");
        pst.setInt(1,m.getJoueur1().getIduser());
        pst.setInt(2,m.getJoueur2().getIduser());
        pst.setInt(3,m.getIdgagnant());
        pst.setInt(4,m.getIdtournoi());
        pst.setString(5,m.getScore());
        pst.setInt(6,m.getPhase());
        pst.setInt(7,m.getPlaceInArray());
        pst.executeUpdate();
        
        PreparedStatement prep = con.prepareStatement("SELECT idmatch FROM rencontre WHERE idtournoi=? AND phase=? AND placeinarray=?");
        prep.setInt(1,m.getIdtournoi());
        prep.setInt(2,m.getPhase());
        prep.setInt(3,m.getPlaceInArray());
        ResultSet rset = prep.executeQuery();
        rset.next();
        return rset.getInt("idmatch");
    }

    public int EmptyTournamentQuery() {
        PreparedStatement prep = null;
        int result = 1;
        try {
            ResultSet rset;
            String rqt = "SELECT * FROM rencontre WHERE idtournoi=?";
            prep = con.prepareStatement(rqt);
            prep.setInt(1,idtournoi);
            rset = prep.executeQuery();
            if (rset.next()) {
                result = 0;
            }
        } catch (SQLException exc) {
            System.out.println(exc.getErrorCode());
        } finally {
            try {
                // la clause finally est toujours executée, quoi qu'il arrive
                if (prep != null) {
                    prep.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getErrorCode());
            }
        }
        return result;
    }
    
    public int TournamentRowCount() {//
        PreparedStatement prep = null;
        int result = 1;
        try {
            ResultSet rset;
            String rqt = "SELECT * FROM rencontre WHERE idtournoi=?";
            prep = con.prepareStatement(rqt);
            prep.setInt(1,idtournoi);
            rset = prep.executeQuery();
            if (rset.next()) {
                result = 0;
            }
        } catch (SQLException exc) {
            System.out.println(exc.getErrorCode());
        } finally {
            try {
                // la clause finally est toujours executée, quoi qu'il arrive
                if (prep != null) {
                    prep.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getErrorCode());
            }
        }
        return result;
    }
}
