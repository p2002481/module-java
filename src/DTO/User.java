package DTO;

//y'aura ptet des méthodes personnalisées à ajouter pour plus tard, gérer arbitre joueur interférences...

import java.util.ArrayList;


public class User {

    private int iduser = 0;
    private String nom = "null";
    private String prenom = "null";    
    private String nationalite;
    private int idtype;
    //private int idequipe = 0;
    private static ArrayList<User> ListUser = new ArrayList();

    public User(){

    }
    
    public User(int iduser, String nom, String prenom, int idtype, String nationalite){
        this.iduser = iduser;
        this.nom = nom;
        this.prenom = prenom;        
        this.idtype = idtype;
        this.nationalite = nationalite;
        ListUser.add(this);
    }
    
    public static ArrayList<User> getUserList(){
        return ListUser;
    }
    /**
     * @return int return the iduser
     */
    public int getIduser() {
        return iduser;
    }

    /**
     * @param iduser the iduser to set
     */
    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    /**
     * @return String return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return String return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return String return the nationalite
     */
    public String getNationalite() {
        return nationalite;
    }

    /**
     * @param nationalite the nationalite to set
     */
    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    /**
     * @return int return the idtype
     */
    public int getIdtype() {
        return idtype;
    }

    /**
     * @param idtype the idtype to set
     */
    public void setIdtype(int idtype) {
        this.idtype = idtype;
    }

    /**

    public int getIdequipe() {
        return idequipe;
    }

    
    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }
    */
}
