/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import DTO.Match;
import DAO.MatchDAO;
import DTO.User;
import DAO.UserDAO;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author antoi
 */
public class TournoiController {
    
    private ArrayList<Match> matchs16 = new ArrayList();
    private ArrayList<Match> matchs8 = new ArrayList();
    private ArrayList<Match> matchs4 = new ArrayList();
    private ArrayList<Match> matchs2 = new ArrayList();
    private static ArrayList<TournoiController> tournoiList = new ArrayList();
    private Match finale = new Match();
    private MatchDAO matchdao;
    public static int idtournoi = 1;
    
    public TournoiController(UserDAO userdao, MatchDAO matchdao){
        Match m;
        User player1, player2;
        ArrayList<User> userList = User.getUserList();
        ArrayList<User> playerList = new ArrayList();
        this.matchdao = matchdao;
        userdao.loadPlayers();
        Collections.shuffle(userList);//random
        int i=0;
        //On récupère une liste de joueur parmi les objets user
        for(User u : userList){
            if(u.getIdtype()==1 && i!=32){
                playerList.add(u);
                i++;
            }
        }
        //On crée les poules, ici les 16eme
        for(int j=0;j<16;j++){
            Collections.shuffle(playerList);
            player1=playerList.get(0);
            playerList.remove(player1);
            Collections.shuffle(playerList);
            player2=playerList.get(0);
            playerList.remove(player2);
            m = new Match(player1, player2);
            m.setPhase(16);
            m.setPlaceInArray(j);
            m.setIdtournoi(idtournoi);
            matchs16.add(m);
        }
        
        for(int j=0;j<8;j++){
            m = new Match();
            m.setPhase(8);
            m.setPlaceInArray(j);
            m.setIdtournoi(idtournoi);
            matchs8.add(m);
        }
        
        for(int j=0;j<4;j++){
            m = new Match();
            m.setPhase(4);
            m.setPlaceInArray(j);
            m.setIdtournoi(idtournoi);
            matchs4.add(m);
        }
        
        for(int j=0;j<2;j++){
            m = new Match();
            m.setPhase(2);
            m.setPlaceInArray(j);
            m.setIdtournoi(idtournoi);
            matchs2.add(m);
        }
        
        finale.setPhase(1);
        finale.setPlaceInArray(0);
        finale.setIdtournoi(idtournoi);
        
        tournoiList.add(this);
    }
    
    public TournoiController(MatchDAO matchdao){
        Match m = null;
        ArrayList<Match> matchList = Match.getMatchList();
        for(int i=0;i<16;i++){
            for(Match match : matchList){
                if(match.getPhase()==16 && match.getPlaceInArray()==i){
                    m=match;
                    break;
                }
            }
            matchList.remove(m);
            matchs16.add(m);
        }
        for(int i=0;i<8;i++){
            for(Match match : matchList){
                if(match.getPhase()==8 && match.getPlaceInArray()==i){
                    m=match;
                    break;
                }
            }
            matchList.remove(m);
            matchs8.add(m);
        }
        for(int i=0;i<4;i++){
            for(Match match : matchList){
                if(match.getPhase()==4 && match.getPlaceInArray()==i){
                    m=match;
                    break;
                }
            }
            matchList.remove(m);
            matchs4.add(m);
        }
        for(int i=0;i<2;i++){
            for(Match match : matchList){
                if(match.getPhase()==2 && match.getPlaceInArray()==i){
                    m=match;
                    break;
                }
            }
            matchList.remove(m);
            matchs2.add(m);
        }
        for(Match match : matchList){
                if(match.getPhase()==1 && match.getPlaceInArray()==0){
                    m=match;
                    break;
                }
            }
        matchList.remove(m);
        finale = m;
        this.matchdao = matchdao;
        tournoiList.add(this);
    }
    
    public static ArrayList<TournoiController> getTournoiList(){
        return tournoiList;
    }
    
    public static int numberOfMatch(int number){
        double d = number/2;//obligé de diviser en passant par un double pour éviter les erreurs (comme en c)
        return (int) d;
    }
    
    public ArrayList<Match> getMatchs16(){
        return matchs16;
    }
    
    public ArrayList<Match> getMatchs8(){
        return matchs8;
    }
    
    public ArrayList<Match> getMatchs4(){
        return matchs4;
    }
    
    public ArrayList<Match> getMatchs2(){
        return matchs2;
    }
    
    public Match getFinale(){
        return finale;
    }
    
    public MatchDAO getMatchDAO(){
        return matchdao;
    }
    
    public User getJoueurByName(ArrayList<Match> list, String[] name){
        User j = null;
        for(Match m : list){
            System.out.println(name[0] + name[1]);
            if(m.getJoueur1().getPrenom().substring(0, 1).toUpperCase().equals(name[0]) && m.getJoueur1().getNom().equals(name[1]) && name.length>0){
                j = m.getJoueur1();
            }
            if(m.getJoueur2().getPrenom().substring(0, 1).toUpperCase().equals(name[0]) && m.getJoueur2().getNom().equals(name[1]) && name.length>0){
                j = m.getJoueur2();
            }
        }
        return j;
    }
    public Match getMatchByJoueur(ArrayList<Match> list, User user){
        Match j = null;
        for(Match m : list){
            if(m.getJoueur1() == user || m.getJoueur2() == user){
                j = m;
            }
        }
        return j;
    }

    public User getJoueurById(ArrayList<Match> list, int id) {
        User j = null;
        for(Match m : list){
            if(m.getJoueur1().getIduser() == id){
                j = m.getJoueur1();
            }
            if(m.getJoueur2().getIduser() == id){
                j = m.getJoueur2();
            }
        }
        return j;
    }
}
