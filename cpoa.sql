-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 25 jan. 2022 à 21:44
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cpoa`
--

-- --------------------------------------------------------

--
-- Structure de la table `rencontre`
--

DROP TABLE IF EXISTS `rencontre`;
CREATE TABLE IF NOT EXISTS `rencontre` (
  `idmatch` int(11) NOT NULL AUTO_INCREMENT,
  `score` varchar(11) NOT NULL DEFAULT '0-0' COMMENT '"x-x"',
  `idgagnant` int(11) DEFAULT NULL,
  `idarbitre` int(11) DEFAULT NULL,
  `idjoueur1` int(11) NOT NULL,
  `idjoueur2` int(11) NOT NULL,
  `placeinarray` int(10) NOT NULL,
  `phase` int(10) NOT NULL,
  `idtournoi` int(10) NOT NULL,
  PRIMARY KEY (`idmatch`),
  KEY `j1_fk` (`idjoueur1`),
  KEY `j2_fk` (`idjoueur2`),
  KEY `arbitre_fk` (`idarbitre`),
  KEY `fk_idtournoi` (`idtournoi`)
) ENGINE=InnoDB AUTO_INCREMENT=1709 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `court` int(11) NOT NULL,
  `joueur` int(11) NOT NULL COMMENT 'on devait réserver avec le nom, mais si on veut faire une clef étrangère sur la table joueur, il faut le faire sur sa clef primaire soit l''id (on entrerea le nom puis fera la jointure dans al requête)',
  `heure debut` int(11) NOT NULL,
  `heure fin` int(11) NOT NULL,
  PRIMARY KEY (`court`,`joueur`),
  KEY `joueur_fk` (`joueur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `idtype` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`idtype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type`
--

INSERT INTO `type` (`idtype`, `role`) VALUES
(1, 'joueur'),
(2, 'arbitre'),
(3, 'ramasseur');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `idtype` int(11) NOT NULL,
  `nationalité` varchar(100) NOT NULL,
  `idequipe` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  KEY `fk_type` (`idtype`),
  KEY `fk_equipe` (`idequipe`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`iduser`, `nom`, `prenom`, `idtype`, `nationalité`, `idequipe`) VALUES
(1, 'albot', 'radu', 1, 'MDA', NULL),
(2, 'alund', 'martin', 1, 'ARG', NULL),
(3, 'baker', 'jamie', 1, 'GBR', NULL),
(4, 'beck', 'andreas', 1, 'GER', NULL),
(5, 'bellucci', 'thomaz', 1, 'BRA', NULL),
(6, 'cachin', 'pedro', 1, 'RAZ', NULL),
(7, 'chen', 'ti', 1, 'TPE', NULL),
(8, 'coco', 'rudy', 1, 'FRA', NULL),
(9, 'daniel', 'taro', 1, 'JPN', NULL),
(10, 'delic', 'amer', 1, 'BIH', NULL),
(11, 'diep', 'florent', 1, 'FRA', NULL),
(12, 'escoffier', 'antoine', 1, 'FRA', NULL),
(13, 'elias', 'gastao', 1, 'POR', NULL),
(14, 'evans', 'daniel', 1, 'GBR', NULL),
(15, 'eubanks', 'christopher', 1, 'USA', NULL),
(16, 'falla', 'alejandro', 1, 'COL', NULL),
(17, 'favrot', 'alexandre', 1, 'FRA', NULL),
(18, 'fognini', 'fabio', 1, 'ITA', NULL),
(19, 'gaio', 'federico', 1, 'ITA', NULL),
(20, 'galovic', 'viktor', 1, 'CRO', NULL),
(21, 'almagro', 'nicolas', 1, 'ESP', NULL),
(22, 'ghem', 'andre', 1, 'BRA', NULL),
(23, 'haase', 'robin', 1, 'NED', NULL),
(24, 'harrison', 'ryan', 1, 'USA', NULL),
(25, 'hemery', 'calvin', 1, 'FRA', NULL),
(26, 'ignatik', 'uladzimir', 1, 'BLR', NULL),
(27, 'istomin', 'denis', 1, 'UZB', NULL),
(28, 'ito', 'tatsuma', 1, 'JPN', NULL),
(29, 'jankovits', 'yannick', 1, 'FRA', NULL),
(30, 'jones', 'greg', 1, 'AUS', NULL),
(31, 'jung', 'jason', 1, 'TPE', NULL),
(32, 'kamke', 'tobias', 1, 'GER', NULL),
(33, 'king', 'kevin', 1, 'USA', NULL),
(34, 'kravchuk', 'konstantin', 1, 'RUS', NULL),
(41, 'bernardes', 'carlos', 2, 'BRA', NULL),
(42, 'dumusois', 'damien', 2, 'BRA', NULL),
(43, 'steiner', 'damian', 2, 'ARG', NULL),
(44, 'moscarella', 'gianluca', 2, 'ITA', NULL),
(45, 'garner', 'jake', 2, 'USA', NULL),
(46, 'keothavong', 'james', 2, 'GBR', NULL),
(47, 'blom', 'john', 2, 'AUS', NULL),
(48, 'nouni', 'kader', 2, 'FRA', NULL),
(49, 'campistol', 'jaume', 2, 'ESP', NULL),
(50, 'nour', 'adel', 2, 'EGY', NULL),
(51, 'messina', 'manuel', 2, 'ITA', NULL),
(52, 'helwerth', 'nico', 2, 'GER', NULL),
(53, 'hughes', 'alison', 2, 'GBR', NULL),
(54, 'zhang', 'juan', 2, 'CHI', NULL),
(55, 'alves', 'mariana', 2, 'POR', NULL),
(56, 'cicak', 'marija', 2, 'CRO', NULL),
(57, 'veljovic', 'marijana', 2, 'SER', NULL),
(58, 'tourte', 'aurelie', 2, 'FRA', NULL),
(59, 'kjendlie', 'julie', 2, 'NOR', NULL),
(60, 'egli', 'andreas', 2, 'SUI', NULL),
(61, 'lasserre', 'anne', 2, 'FRA', NULL),
(62, 'bowron', 'bertie', 2, 'GBR', NULL),
(63, 'lipp', 'billy', 2, 'USA', NULL),
(64, 'strachan', 'bob', 2, 'AUS', NULL),
(65, 'olaussen', 'christina', 2, 'DAN', NULL),
(66, 'rebeuh', 'bruno', 2, 'FRA', NULL),
(67, 'molina', 'enric', 2, 'ESP', NULL),
(68, 'hammond', 'frank', 2, 'USA', NULL),
(69, 'dias', 'jorge', 2, 'POR', NULL),
(70, 'grillotti', 'romano', 2, 'ITA', NULL),
(71, 'peick', 'norbert', 2, 'GER', NULL),
(72, 'doe', 'john', 3, 'USA', NULL),
(73, 'castle', 'frank', 3, 'USA', NULL),
(74, 'murdock', 'matt', 3, 'USA', NULL),
(75, 'marquina', 'sergio', 3, 'ESP', NULL),
(76, 'arouet', 'françois', 3, 'FRA', NULL),
(77, 'abramovich', 'roman', 3, 'RUS', NULL),
(78, 'padoue', 'antoine', 3, 'POR', NULL),
(79, 'diogo', 'jota', 3, 'POR', NULL),
(80, 'bismarck', 'otto', 3, 'GER', NULL),
(81, 'goethe', 'johann', 3, 'GER', NULL),
(82, 'rivia', 'geralt', 3, 'UZB', NULL),
(83, 'cintra', 'cirilla', 3, 'GBR', NULL),
(84, 'bard', 'jaskier', 3, 'ESP', NULL),
(85, 'vengerberg', 'yennefer', 3, 'CHI', NULL),
(86, 'emreis', 'emhyr', 3, 'GER', NULL),
(87, 'earl', 'anton', 3, 'SUE', NULL),
(88, 'champion', 'andréas', 3, 'FRA', NULL),
(89, 'durif', 'pierre', 3, 'BSN', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `court_fk` FOREIGN KEY (`court`) REFERENCES `court` (`idcourt`),
  ADD CONSTRAINT `joueur_fk` FOREIGN KEY (`joueur`) REFERENCES `user` (`iduser`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_equipe` FOREIGN KEY (`idequipe`) REFERENCES `equipe` (`idequipe`),
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`idtype`) REFERENCES `type` (`idtype`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
