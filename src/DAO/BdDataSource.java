/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import java.sql.*;

/**
 *
 * @author theze
 */
public class BdDataSource {
    
    public Connection dbConnection(){
        Connection con = null;
        String BD = "cpoa";
        String serveur = "localhost:3306/" + BD;
        String user = "root";
        String pwd = "";
        try{
            Class.forName("com.mysql.cj.jdbc.Driver"); 
            con = DriverManager.getConnection("jdbc:mysql://" + serveur, user, pwd);
        }
        catch (Exception e){
            System.out.println("Erreur dans la connexion");
            System.exit(0);
        }
        return con;
    }
}
