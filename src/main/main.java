/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import DAO.BdDataSource;
import DAO.MatchDAO;
import DAO.UserDAO;
import java.sql.Connection;
import Views.JPanelAccueil;
/**
 *
 * @author antoi
 */
public class main {
    public static void main (String args[]){
        
        Connection con;
        BdDataSource bd = new BdDataSource();
        con = bd.dbConnection();
        
        UserDAO userDAO = new UserDAO(con);
        
        MatchDAO matchDAO = new MatchDAO(con);
        
        JPanelAccueil frame = new JPanelAccueil();
        frame.setVisible(true);
    }
}
